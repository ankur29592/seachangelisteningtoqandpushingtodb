package com.alticeusa.SeachangeQListener.junit;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.alticeusa.SeachangeQListener.dao.VODSeachangeConnectionDao;
import com.alticeusa.SeachangeQListener.queue.SeachangeInputReceiver;

@Category(IntegrationTest.class)
public class SeachangeReceiverTest {
	
	static String refNo="";
	static String refInputMsg="";
	static String ackResponse="";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		refNo="IR:39456785644";
		refInputMsg="A001ITT,IR:39456785644                       ,SI:811,S#:SER150892               ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Breaking Bad                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		//ackResponse="I000ITT,000,IR:284446598,AN:000000000.\\r";
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Ignore
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
//	@Test
//	public void testIsValidInputCase1() {
//		assertTrue(SeachangeInputReceiver.isValidInput("start"));		
//	}
//	
//	@Test
//	public void testIsValidInputCase2() {
//		assertTrue(SeachangeInputReceiver.isValidInput("stop"));		
//	}
//	
//	@Test
//	public void testIsContextInitialized() {
//		assertFalse(SeachangeInputReceiver.contextInitialized());		
//	}
//	
	@Test
	public void testGetRefNoCase1() {
		assertEquals(refNo, SeachangeInputReceiver.getRefNo(refInputMsg));
	}
	
	@Test
	public void testGetRefNoCase2() {
		assertEquals("", SeachangeInputReceiver.getRefNo(null));
	}
	
//	@Test
//	public void connectdatabase() throws ClassNotFoundException, SQLException {		
//		Connection result = VODSeachangeConnectionDao.getDBConnection();
//		System.out.println("result: "+result.toString());
//		//assertEquals("oracle.jdbc.driver.T4CConnection@7cc0cdad", VODSeachangeConnectionDao.getDBConnection().toString());
//	}

}
