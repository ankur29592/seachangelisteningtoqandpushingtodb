package com.alticeusa.SeachangeQListener.dao;

import com.alticeusa.SeachangeQListener.utils.Constants;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class VODSeachangeConnectionDao extends BaseDAO {

	private static final Logger logger = LogManager.getLogger(VODSeachangeConnectionDao.class);	
	public static VODSeachangeConnectionDao instance = null;
	public static Connection dbcon = null;
	public static VODSeachangeConnectionDao getInstance() throws Exception  {
		if (instance == null)
		{
			instance = new VODSeachangeConnectionDao();
		}//if
		return instance;
	}
	
	public static Connection getDBConnection() throws ClassNotFoundException, SQLException {		
		try {
			Class.forName(Constants.DATABASE_CONNECT);  
			dbcon=DriverManager.getConnection(Constants.DATABASE_URL,Constants.DATABASE_USERNAME,Constants.DATABASE_PASSWORD);  						
		} catch (SQLException e) {
			if(e.getMessage()!=null)
				logger.debug("SQLException occured while creating connection from DataSource :"+e.getMessage());			
		}
		return dbcon;
	}
}
