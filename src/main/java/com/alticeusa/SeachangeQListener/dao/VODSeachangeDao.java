package com.alticeusa.SeachangeQListener.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.SeachangeQListener.dao.VODSeachangeDao;
import com.alticeusa.SeachangeQListener.dao.BaseDAO;
import com.alticeusa.SeachangeQListener.dao.DBInterface;
import com.alticeusa.SeachangeQListener.utils.Constants;


public class VODSeachangeDao extends BaseDAO implements DBInterface {
	static Connection con = null;
	static CallableStatement csmt = null;
	static VODSeachangeConnectionDao vodSeachangeConnectionDao;
	private static final Logger logger = LogManager.getLogger(VODSeachangeDao.class);

	public static VODSeachangeDao instance = null;

	public static VODSeachangeDao getInstance() throws Exception {
		if (instance == null) {
			instance = new VODSeachangeDao();
		}
		return instance;
	}

	
	public static int insert_vod_seachange_input(String input,int threshold) throws Exception {
		try {					
				vodSeachangeConnectionDao= VODSeachangeConnectionDao.getInstance();
				con=VODSeachangeConnectionDao.getDBConnection();
				logger.debug(" execute proc(): "+INSERT_FOR_VODSEACHANGE);				
				csmt = con.prepareCall(INSERT_FOR_VODSEACHANGE);
				csmt.setString(1, input);			
				csmt.registerOutParameter(2, java.sql.Types.INTEGER);
				csmt.registerOutParameter(3, java.sql.Types.VARCHAR);
				csmt.executeUpdate();			
				int errorCode = csmt.getInt(2);
				String errorMsg = csmt.getString(3);			
				logger.debug(">>>>> VODSeachange <" + "> code <" + errorCode + "> message <" + errorMsg + ">");
				return errorCode;
		 }
		  catch (SQLException e) {			  
			if(threshold > 0) {
				threshold--;
				logger.debug("Retry No::"+threshold);
				insert_vod_seachange_input(input,threshold);
				int waittime = Integer.parseInt(Constants.WAIT_TIME);
				Thread.sleep(waittime);
			} else {
				throw e;				
			}
			 return 0;
		} catch (Exception e) {			
			if(threshold > 0) {
				threshold--;
				logger.debug("No of Retry::"+threshold);
				insert_vod_seachange_input(input,threshold);
				int waittime = Integer.parseInt(Constants.WAIT_TIME);
				Thread.sleep(waittime);
			} else {
				throw e;				
			}
			 return 0;
		} finally {
			if (con != null) {

				try {
					if (csmt != null)
						csmt.close();
					con.close();
				} catch (SQLException e) {					
				}

			}
		}
	}
}
