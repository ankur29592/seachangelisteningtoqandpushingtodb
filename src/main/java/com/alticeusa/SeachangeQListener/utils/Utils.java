package com.alticeusa.SeachangeQListener.utils;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.logging.log4j.*;
//import org.apache.logging.log4j.Logger;

import com.alticeusa.SeachangeQListener.metrics.log.SeachangeListenerLoggerHelper;


public class Utils {

	private static final Logger logger = LogManager.getLogger(Utils.class);
	
	
	
	public static String getCurrentTimeStamp(String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date now = new Date();
		String formattedDateTime = dateFormat.format(now);
		return formattedDateTime;
	}
	
	public static void performMetricsLogging(String message) {
		SeachangeListenerLoggerHelper metricsData = new SeachangeListenerLoggerHelper(message);
		logger.info(metricsData.toString());
	}
	
	public static void performNecessaryLogging(String request,String errorStackTrace, long startTime) {
		long endTime = System.currentTimeMillis();
		performApplicationLogging(request, startTime, endTime);
		performMetricsLogging(request);
	}

	
	public static void performApplicationLogging(String request, long startTime, long endTime) {
		long elapsed = endTime - startTime;		
		//logger.info("<<< SeachangeInputReceiver() [ Time taken = {} milli-seconds] ::: request = {}", elapsed,request);
		logger.info("<<SeachangeInputReceiver() [ Time taken = {} milli-seconds] ::: request = {}", elapsed);
	}

	
	public static Map<String, String> parseMessage(String message) {
		Map<String, String> hmParsedMsg = new HashMap<>();
		StringTokenizer st = new StringTokenizer(message, ",");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.contains(":")) {
				String key = token.substring(0, token.indexOf(":"));
				String val = token.substring(token.indexOf(":") + 1);
				hmParsedMsg.put(key.trim(), val.trim());					
			}
		}
		return hmParsedMsg;
	}

}
