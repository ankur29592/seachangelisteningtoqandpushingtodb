package com.alticeusa.SeachangeQListener.utils;

import java.util.regex.Pattern;

import com.alticeusa.SeachangeQListener.utils.PropertiesReader;
import com.alticeusa.SeachangeQListener.utils.StringFormatter;

/**
 * Constant class for VODSeachange.
 * @author Pooja Sontakke
 * @version 1.0
 * @since 2018-02-02
 */
public final class Constants {
	private static PropertiesReader prop = PropertiesReader.getInstance();
	public static final String SEACHANGE_QUEUE = prop.getValue("SEACHANGE_JMS_QUEUE");
	public static final String SEACHANGE_BROKER_URL = prop.getValue("SEACHANGE_BROKER_URL");
	public static final String DATABASE_CONNECT = prop.getValue("DATABASE_CONNECT");
	public static final String DATABASE_URL = prop.getValue("DATABASE_URL");
	public static final String DATABASE_USERNAME = prop.getValue("DATABASE_USERNAME");
	public static final String DATABASE_PASSWORD = prop.getValue("DATABASE_PASSWORD");
	public static final String MAX_RETRY = prop.getValue("MAX_RETRY");
	public static final String WAIT_TIME = prop.getValue("WAIT_TIME");
	
	//Metrics Logging
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_FORMAT = "YYYY-MM-dd'T'HH:mm:ss.SSS";
	public static final String EMPTY_JSON= "{}";
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_KEY= "Datetime";
	public static final String METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY = "Thread";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY = "contextName";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY = "contextName";
	public static final String METRICS_LOGGING_REPEATER_LOG_ENTRY_TYPE = "LogEntryType";
	public static final String METRICS_LOGGING_COMMON_CLASS_NAME_KEY = "ClassName";
	public static final String METRICS_LOGGING_COMMON_CLASS_NAME_VALUE = "";
	public static final String METRICS_LOGGING_CLASS_NAME_VALUE = "com.alticeusa.SeachangeQListener.server.SeachangeQListener";
	public static final String METRICS_LOGGING_COMMON_METHOD_NAME_KEY = "MethodName";
	public static final String METRICS_LOGGING_METHOD_NAME_VALUE = "handleSelectionKeys";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE ="VODSeachange";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE = "SeachangeQListener";
	public static final String METRICS_LOGGING_COMMON_METHOD_NAME_VALUE = "readRequest";
	public static final String METRICS_LOGGING_COMMON_ERROR_CODE="ErrorCode";
	public static final String METRICS_LOGGING_COMMON_ERROR_MESSAGE= "ErrorMessage";
	public static final String METRICS_LOGGING_COMMON_STATE="State";
	public static final String RESPONSE = "Response";
	public static final String REQUEST = "Request";
	public static final Pattern maskRegex = Pattern.compile("\\b[0-9]{9}\\b|\\b[0-9]{16}\\b|\\b[0-9]{3}-[0-9]{2}-[0-9]{4}\\b");
	public static final String SSN_MASK="XXXXXXXXX";
}
