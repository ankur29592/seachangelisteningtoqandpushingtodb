package com.alticeusa.SeachangeQListener.queue;

import java.util.HashMap;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.alticeusa.SeachangeQListener.dao.VODSeachangeDao;
import com.alticeusa.SeachangeQListener.utils.Constants;
import com.alticeusa.SeachangeQListener.utils.Utils;



public class SeachangeInputReceiver {

	private static final Logger logger = LogManager.getLogger(SeachangeInputReceiver.class);
	
	private static  Connection connection=null;
	private static ConnectionFactory factory =null;
	private static Destination destination =null;
	private static Session session =null;
    private static MessageConsumer consumer =null;
    private static MessageProducer producer =null;
    
   
    public static void contextDestroyed() {
		try {
			if (consumer != null) {
				consumer.close();
			}
			if (session != null) {
				session.close();
			}
			if (connection != null) {
				connection.close();
			}
			if (producer != null) {
				producer.close();
			}
			ThreadContext.clearMap();
		}catch (JMSException e) {
			e.printStackTrace();
		}
	}
    
    public static  void contextInitialized() {	    	
    	logger.info(">>>>> In VODSeachange reciever Queue contextInitialized");
		try {			
			factory =new ActiveMQConnectionFactory(Constants.SEACHANGE_BROKER_URL);
    		connection = factory.createConnection();
    		connection.start();
    		session=connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    		destination=session.createQueue(Constants.SEACHANGE_QUEUE);
    		consumer = session.createConsumer(destination);       
    		logger.info("VODSeachange reciever Queue initialized successfully ..!! ");
		} catch (JMSException e) {			
			logger.info("Error in contextInitialized of SeachangeInputReceiver: " +e);			
		}		  
	}
    
    
    public static String getRefNo(String msg) {		
		String IRNo="";
		if (msg != null && !"".equals(msg.trim())) {
			int index = msg.indexOf("IR:");
			IRNo = msg.substring(index, msg.indexOf(",", index));
			logger.debug("Ref No received = " + IRNo.trim());
		} else {
			logger.info(("Msg is null or empty"));
		}		
		return IRNo.trim();
	}
    
    
    
    
        
    public static void main (String arg[]) throws Exception{    	        	
    	while (true){      		
    		String getmessage = null;
    		String refno = null;    	
    		long start = System.currentTimeMillis();
    		String errorStackTrace=null;
    		try
        	{    	    			
    			logger.info(" ------------ SeachangeQListener start ------------");
    			contextInitialized();
    			int threshold = Integer.parseInt(Constants.MAX_RETRY);
        		Message message =consumer.receive();
        		if (message instanceof TextMessage) {
        			TextMessage text = (TextMessage) message;
        			getmessage = text.getText();
        			
        			//parsing message
        			HashMap<String, String> hmParsedMessage = (HashMap<String, String>) Utils.parseMessage(getmessage);
        			//ThreadContext
        			logger.info("hmParsedMessage IR:"+hmParsedMessage.get("IR"));
        			ThreadContext.put("IR", hmParsedMessage.get("IR"));
        			ThreadContext.put("corp", hmParsedMessage.get("SI"));        			
        			if(hmParsedMessage.get("EA")!=null)
        				ThreadContext.put("equipmentAddress", hmParsedMessage.get("EA"));
        			else
        				ThreadContext.put("equipmentAddress", "");
        			
        			if(hmParsedMessage.get("S#")!=null)
        				ThreadContext.put("serialNumber", hmParsedMessage.get("S#"));
        			else
        				ThreadContext.put("serialNumber", "");
        			
        			Utils.performMetricsLogging(getmessage);
        			Utils.performNecessaryLogging(getmessage,errorStackTrace,start);
        			
        			
        			if (!getmessage.isEmpty() || getmessage != null ){
	        			refno = getRefNo(getmessage);        			
	        			logger.debug("Message received for refernceID ="+refno+ "\n " + text.getText());    			
	        			logger.debug(">>>>>>>>> Sending Message to Database .....");        			
	        			int errorCode = VODSeachangeDao.insert_vod_seachange_input(getmessage,threshold);
	        			if (errorCode == 0){    				
	        				logger.info(" Record inserted successfully ");	
	        			}
	        			else{		
	        				logger.info(" Error whie inserting record.");
	        			} 
        		   }
        		 }
        		ThreadContext.clearMap();
        	}
    		catch (NullPointerException e) {        		
        		logger.error("NullPointerException while processing message for refernceID=" +refno + " "+ getmessage);         		        			
        		}
        	catch (Exception e) {        		
        		logger.error("Error while processing message for refernceID=" +refno + " "+ getmessage);         		        			
        		}
        	finally{ contextDestroyed();}    
    		Utils.performNecessaryLogging(getmessage,errorStackTrace,start);
        	logger.info("<<<<< Out of VODSechange reciever Queue contextInitialized");
        	logger.info(" ------------ SeachangeQListener stop  ------------\n\n");
    	}
    } 
    
}
